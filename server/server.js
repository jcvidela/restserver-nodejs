require("./config/config");

const express = require("express");
const mongoose = require("mongoose");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");

//config middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//habilitar vista principal
app.use(express.static(path.resolve(__dirname, "../public")));

//config de controladores
app.use(require("./controller/index"));

//coneccion con mongoDB
mongoose.connect(process.env.urlDB, 
{
  useNewUrlParser: true,
  useUnifiedTopology: true,
}, (err, resp) => {
  if (err) throw err;
  console.log("DB running!");
});

mongoose.set('useFindAndModify', false);
app.listen(process.env.PORT, () => {
  console.log("Escuchando puerto: ", process.env.PORT);
});
