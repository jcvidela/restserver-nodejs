//Definimos un puerto
process.env.PORT = process.env.PORT || 3000;

//Configurar entorno
process.env.NODE_ENV = process.env.NODE_ENV || "dev";

//base de datos cloud
let urlDB;
if (process.env.NODE_ENV === "dev") {
  urlDB = "mongodb://localhost:27017/cafe";
} else {
    urlDB = process.env.MONGO_URI;
}
process.env.urlDB = urlDB;

process.env.EXPIRACION_TOKEN = '48h'; 

//Seed de autenticacion
process.env.SEED = process.env.SEED || 'este-es-el-seed-de-desarrollo'; 

//Google Client ID
process.env.CLIENT_ID = process.env.CLIENT_ID || "79178500148-sj5n394bai8on4gcaku0a23taudprkuj.apps.googleusercontent.com";