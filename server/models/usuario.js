const mongoose = require("mongoose");
//importamos la lista de roles admitidos
let whiteListRoles = require('../../whiteListRoles');
const uniqueValidator = require("mongoose-unique-validator");
mongoose.set('useCreateIndex', true);

let Schema = mongoose.Schema;

// definimos el schema y propiedades de un usuario en la db
let usuarioSchema = new Schema({
  nombre: {
    type: String,
    unique: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "El email es obligatorio", { uniqueValidator: true }],
  },
  password: {
    type: String,
    required: [true, "La contraseña es obligatoria"],
  },
  img: {
    type: String,
    required: false,
  },
  role: {
    type: String,
    required: [true, "El role es obligatorio"],
    default: "USER_ROLE",
    enum: whiteListRoles
  },
  estado: {
    type: Boolean,
    default: true,
    required: [true, "El estado es obligatorio"],
  },
  googleAuth: {
    type: Boolean,
    default: false,
  },
});

//borramos la contrasenha del objeto usuario (no afecta en la creacion)
usuarioSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    
    delete userObject.password;

    return userObject;
}

//plugin de mensaje de error
usuarioSchema.plugin(uniqueValidator, { message: "{PATH} deber ser unico" });
//exportamos el modelo llamado Usuario con la config del schema usuario
module.exports = mongoose.model("Usuario", usuarioSchema);
