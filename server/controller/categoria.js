const express = require("express");
const app = express();
const _ = require("underscore");

//MODELS
const Categoria = require("../models/categoria");

const { verificarToken, verificarRole } = require("../middlewares/autenticacion");

//=============================================== ABM USUARIOS ===============================================

app.get("/categoria", verificarToken, (req, res) => {
  let desde = Number(req.query.desde) || 0;
  let limite = Number(req.query.limite) || 5;

  Categoria.find({}, "descripcion usuario")
    .skip(desde)
    .limit(limite)
    .sort("descripcion")
    .populate("usuario", "nombre email")
    .exec((err, categoriaDB) => {
    if (err) return res.status(500).json({ ok: false, err });

    if (!categoriaDB) {
      res
        .status(400)
        .json({ ok: false, err: { message: "No se obtuvieron categorias" } });
    }

    Categoria.countDocuments((err, count) => {
      res.json({
        ok: true,
        categoria: categoriaDB,
        cantidad: count,
      });
    });
  });
});

app.get("/categoria/:id", verificarToken, (req, res) => {
  let idCategoria = req.params.id;

  Categoria.findById(idCategoria, (err, categoriaDB) => {
    if (err) return res.status(500).json({ ok: false, err });

    if (!categoriaDB) {
      res
        .status(400)
        .json({ ok: false, err: { message: "No hay categorias con ese id" } });
    }

    res.json({ ok: true, categoria: categoriaDB });
  });
});

app.post("/categoria", verificarToken, (req, res) => {
  let body = req.body;

  let categoria = new Categoria({
    descripcion: body.descripcion,
    usuario: req.usuario._id,
  });

  categoria.save((err, categoriaDB) => {
    if (err) return res.status(500).json({ ok: false, err });

    if (!categoriaDB) {
      res.status(400).json({
        ok: false,
        err: { message: "La categoria no pudo ser creada" },
      });
    }

    res.json({ ok: true, categoria: categoriaDB });
  });
});

app.put("/categoria/:id", verificarToken, (req, res) => {
  let idCategoria = req.params.id;
  let usuario = req.usuario._id;
  let descripcion = req.body.nombre;
  let data = { usuario, descripcion };

  Categoria.findByIdAndUpdate(
    idCategoria,
    data,
    {
      new: true,
      runValidators: true,
      context: "query",
    },
    (err, categoriaDB) => {
      if (err) return res.status(500).json({ ok: false, err });
      if (!categoriaDB)
        return res
          .status(400).json({
            ok: false,
            err: { message: "No hay categorias con ese id" },
          });

      res.send({ ok: true, categoria: categoriaDB });
    }
  );
});

app.delete("/categoria/:id", [verificarToken, verificarRole], (req, res) => {
  let idCategoria = req.params.id;

  Categoria.findByIdAndRemove(idCategoria, (err, categoriaDB) => {
    if (err) return res.status(500).json({ ok: false, err });
    if (!categoriaDB)
      return res.status(400)
        .json({ ok: false, err: { message: "No hay categorias con ese id" } });

    res.send({ ok: true, categoria_eliminada: categoriaDB });
  });
});
//=============================================== ABM USUARIOS ===============================================

module.exports = app;
