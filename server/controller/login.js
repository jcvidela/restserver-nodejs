const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID);
const Usuario = require("../models/usuario");

/**
 * Verificamos que el token recibido concuerde con el clientId de
 * la configuracion de la API y obtenemos el payload, si es correcto
 * devolvemos los datos del payload en un objeto
 * @async
 * @param {*} token
 */
const verify = async (token) => {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  });
  const payload = ticket.getPayload();
  return {
    nombre: payload.name,
    email: payload.email,
    img: payload.picture,
    google: true,
  };
};

app.post("/login", (req, res) => {
  let body = req.body;

  Usuario.findOne({ email: body.email }, (error, usuarioDB) => {
    if (error) return res.status(500).json({ ok: false, error });

    if (!usuarioDB) {
      return res.status(400).json({
        ok: false,
        err: { message: "(Usuario) o contraseña invalidos" },
      });
    } 
    if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
      return res.status(400).json({
        ok: false,
        err: { message: "Usuario o (contraseña) invalidos" },
      });
    }

    //generamos token
    let token = jwt.sign(
      { usuario: usuarioDB },  process.env.SEED,
      { expiresIn: process.env.EXPIRACION_TOKEN }
    );

    res.json({ ok: true, usuario: usuarioDB, token });
  });
});

app.post("/google", async (req, res) => {
  let token = req.body.idtoken;
  //validamos el token...
  let googleUser = await verify(token).catch((error) => {
    return res.status(403).json({ ok: false, error });
  });

  //si el token es correcto buscamos al usuario en la DB
  Usuario.findOne({ email: googleUser.email }, (error, usuarioDB) => {
    if (error) res.status(500).json({ ok: false, error });
   
    if (usuarioDB) {
      if (usuarioDB.googleAuth === false) {
        let textError = "Este email ya fue registrado en una cuenta normal";
        res.status(400).json({ ok: false, message: textError });
      } else {
        //generamos token
        let token = jwt.sign(
          {
            suario: usuarioDB,
          },
          process.env.SEED,
          { expiresIn: process.env.EXPIRACION_TOKEN }
        );
        res.json({ ok: true, usuario: usuarioDB, token });
      }
    }

    //si es un nuevo usuario, lo registramos con google
    else {
      let usuario = {
        nombre: googleUser.nombre,
        email: googleUser.email,
        img: googleUser.img,
        googleAuth: true,
        password: ":)",
      };

      usuario.save((error, usarioDB) => {
        if (error) res.status(500).json({ ok: false, error });

        //generamos token
        let token = jwt.sign(
          { suario: usuarioDB }, process.env.SEED,
          { expiresIn: process.env.EXPIRACION_TOKEN }
        );

        res.json({ ok: true, usuario: usarioDB, token });
      });
    }
  });
});

module.exports = app;
