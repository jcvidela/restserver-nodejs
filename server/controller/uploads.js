const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();
const fs = require("fs");
const path = require("path");

const { verificarToken } = require("../middlewares/autenticacion");

const Usuario = require("../models/usuario");
const Producto = require("../models/producto");

//middleware files
app.use(fileUpload({
  useTempFiles: true,
  tempFileDir: '/tmp/'
}));

app.put("/upload/:tipo/:id", verificarToken, (req, res) => {
  //definimos extensiones y tipos validos
  const tiposValidos = ["productos", "usuarios"];
  const extensionesPermitidas = ["png", "jpeg", "jpg", "pdf", "gif"];
  //obtenemos el tipo de imagen/archivo(producto/usuario) y el id del usuario que ejecuto la acción
  let tipo = req.params.tipo;
  let id = req.params.id;

  //si no enviaron archivos...
  if (!req.files) {
    return res
      .status(400)
      .json({ ok: false, err: { message: "No se subieron archivos" } });
  }

  //si no es un tipo de archivo permitido...
  if (tiposValidos.indexOf(tipo) < 0) {
    return res
      .status(400)
      .json({ ok: false, err: { message: "tipo invalido" } });
  }

  //obtenemos el archivo y definimos el nombre del archivo, la extension del mismo
  let archivo = req.files.archivo;
  let nombreArchivo = archivo.name.split(".");
  let archivoExtension = nombreArchivo[nombreArchivo.length - 1];

  //si no es una extension permitida...
  if (extensionesPermitidas.indexOf(archivoExtension) < 0) {
    return res
      .status(400)
      .json({ ok: false, err: { message: "extensión invalida" } });
  }

  //definimos un nombre de archivo único => (idUsuario) - (milisegundos) - (extension del archivo)
  let nuevoNombreArchivo = `${id}-${new Date().getMilliseconds()}.${archivoExtension}`;
  
  //subimos el archivo al directorio correspondiente
  archivo.mv(`uploads/${tipo}/${nuevoNombreArchivo}`, (err) => {
    if (err) return res.status(500).json({ ok: false, err });

    //si es tipo usuario...
    if(tipo === "usuarios"){
        imagenUsuario(id, res, nuevoNombreArchivo);
    } 
    //si es tipo producto...
    else if(tipo === "productos"){
        imagenProducto(id, res, nuevoNombreArchivo);
    }
  });
});

//usuario
function imagenUsuario(id, res, nombreArchivo) {
  Usuario.findById(id, (err, usuarioDB) => {
    if (err) {
      borrarArchivo(usuarioDB.img, "usuarios");
      return res.json(500).json({ ok: false, err });
    }

    if (!usuarioDB) {
      borrarArchivo(usuarioDB.img, "usuarios");
      return res.status(400)
        .json({ ok: false, err: { message: "El usuario no existe" } });
    }
    
    borrarArchivo(usuarioDB.img, "usuarios");
    usuarioDB.img = nombreArchivo;
    
    usuarioDB.save((err, usuarioGuardado) => {
      res.json({ ok: true, usuario: usuarioGuardado, img: nombreArchivo });
    });
  });
}

//producto
function imagenProducto(id, res, nombreArchivo){
  Producto.findById(id, (err, productoDB) => {
    if (err) {
      borrarArchivo(productoDB.img, "productos");
      return res.json(500).json({ ok: false, err });
    }

    if (!productoDB) {
      borrarArchivo(productoDB.img, "productos");
      return res.status(400)
        .json({ ok: false, err: { message: "El producto no existe" } });
    }
    
    borrarArchivo(productoDB.img, "productos");
    productoDB.img = nombreArchivo;
    
    productoDB.save((err, usuarioGuardado) => {
      res.json({ ok: true, usuario: usuarioGuardado, img: nombreArchivo });
    });
  });
}

function borrarArchivo(nombreImagen, tipo) {
  //definimos la ruta donde se guardan los archivos
  let pathImagen = path.resolve(__dirname,`../../uploads/${tipo}/${nombreImagen}`);

  //borramos el archivo
  if (fs.existsSync(pathImagen)) {
    fs.unlinkSync(pathImagen);
  }
}

module.exports = app;
