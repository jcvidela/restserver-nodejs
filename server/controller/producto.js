const express = require("express");
const app = express();
const _ = require("underscore");

//MODELS
const Producto = require("../models/producto");
const Categoria = require("../models/categoria");

const { verificarToken } = require("../middlewares/autenticacion");

//=============================================== ABM USUARIOS ===============================================
app.get("/producto", verificarToken, (req, res) => {
  //obtenemos las query y les definimos un default value
  let desde = Number(req.query.desde) || 0;
  let limite = Number(req.query.limite) || 5;

  Producto.find({ disponible: true }, "disponible nombre descripcion precioUni")
  .skip(desde)
  .limit(limite)
  .populate("usuario", "nombre email")
  .populate("categoria", "descripcion")
  .exec((err, productoDB) =>{
    if(err) return res.status(500).json({ ok:false, err })
    if(!productoDB){
      return res.status(400)
        .json({ok:false, err:{message: "No hay productos disponibles"}})
    }
    
    Producto.countDocuments({ disponible: true }, (err, count) =>{
      res.json({
        ok: true,
        producto: productoDB,
        cantidad: count
      })
    })
  })

});

app.get("/producto/:id", verificarToken, async (req, res) => {
  let id = req.params.id;
  Producto.findById(id, (err, productoDB) =>{
    if(err) return res.status(500).json({ok:false, err})
    if(!productoDB) {
      return res.status(400)
      .json({ok:false, err:{message: "No hay usuarios par con ese id"}})
    }

    res.json({ ok: true, usuario: productoDB })
  })
});

app.post("/producto", verificarToken, async (req, res) => {
  let body = req.body;

  //verificamos el tipo de id recibido
  if (!body.categoria.match(/^[0-9a-fA-F]{24}$/)) {
    return res.status(400)
    .json({ok:false, err:{message: "El formato de id recibido es inválido"}})
  } 

  //buscamos en la db la categoria y validamos..
  let categoria = await Categoria.findById(body.categoria)
  if(!categoria){
    return res.status(400)
    .json({ok: false, err: {message: "La categoria recibida es inválida"}})
  }

  let producto = new Producto({
    nombre: body.nombre,  
    descripcion: body.descripcion,  
    precioUni: body.precioUnitario,  
    usuario: req.usuario._id,
    categoria
  })

  producto.save((err, productoDB) =>{
    if(err)return res.status(500).json({ok:false, err})
    if(!productoDB){
      return res.status(400)
      .json({ok:false, err:{message: "No se pudo crear el producto"}})
    }

    res.json({ ok: true, producto: productoDB });
  })
});

app.put("/producto/:id", verificarToken, async (req, res) => {
  let id = req.params.id;
  let body = req.body;

  Producto.findById(id, (err, productoDB) =>{
    if(err) return res.status(500).json({ok:false, err});
    if(!productoDB){
      return res.status(400)
        .json({ ok:false, err:{message: "No existe producto con ese id"}})
    }
    
    productoDB.nombre = body.nombre ||  productoDB.nombre,
    productoDB.descripcion = body.descripcion || productoDB.descripcion,
    productoDB.precioUni = body.precioUni || productoDB.precioUni,
    productoDB.categoria = body.categoria || productoDB.categoria

    productoDB.save((err, productoGuardado) =>{
      if(err) return res.status(500).json({ok:false, err});
      res.send({ ok: true, producto: productoGuardado });
    })
  })
});

app.get("/producto/buscar/:termino", verificarToken, (req, res) =>{
  let termino = req.params.termino;
  let regex = new RegExp(termino, 'i')

  Producto.find({ nombre: regex, disponible: true })
  // .populate('categoria', 'descripcion')
  .exec((err, productosDB) =>{
    if(err) return res.status(500).json({ok: false, err})
    if(err) return res.status(400).json({ok: false, err:{message: "no se encontraron productos"}})

    res.json({
      ok: true,
      productos: productosDB
    })
  });
})

//cambiar disponibilidad del estado
app.delete("/producto/:id", verificarToken, (req, res) => {
  let id = req.params.id;
  
  Producto.findById(id, (err, productoDB) =>{
    if(err) return res.status(500).json({ok:false, err});
    if(!productoDB){
      return res.status(400)
        .json({ ok:false, err:{message: "No existe producto con ese id"}})
    }
    
    productoDB.disponible = false;

    productoDB.save((err, productoDesactivado) =>{
      if(err) return res.status(500).json({ok:false, err});
      res.send({ ok: true, producto: productoDesactivado });
    })
  })
});
//=============================================== ABM USUARIOS ===============================================

module.exports = app;













