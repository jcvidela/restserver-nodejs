const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const _ = require("underscore");

//MODELS
const Usuario = require("../models/usuario");

const { verificarToken, verificarRole } = require("../middlewares/autenticacion");

//=============================================== ABM USUARIOS ===============================================

app.get("/usuario", verificarToken, (req, res) => {
  //obtenemos las query y les definimos un default value
  let desde = Number(req.query.desde) || 0;
  let limite = Number(req.query.limite) || 5;

  //traemos los campos seleccionados de los usuarios activos
  Usuario.find({ estado: true }, "nombre email img role estado googleAuth")
    .skip(desde)
    .limit(limite)
    .exec((err, users) => {
      if (err) return res.status(500).json({ ok: false, err });
      if (!users)
        return res
          .status(400)
          .json({ ok: false, err: { message: "No hay usuarios con ese id" } });

      Usuario.countDocuments({ estado: true }, (err, count) => {
        res.json({
          ok: true,
          users,
          cantidad: count,
        });
      });
    });
});

app.post("/usuario", [verificarToken, verificarRole], (req, res) => {
  let body = req.body;

  //definimos un nuevo usuario
  let usuario = new Usuario({
    nombre: body.nombre,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    role: body.role,
    img: body.img,
  });

  //guardamos al nuevo usuario
  usuario.save((err, usuarioDB) => {
    //validamos...
    if (err) return res.status(500).json({ ok: false, err });
    if (!usuarioDB)
      return res
        .status(400)
        .json({ ok: false, err: { message: "No hay usuarios con ese id" } });

    res.json({ ok: true, usuario: usuarioDB });
  });
});

app.put("/usuario/:id", [verificarToken, verificarRole], (req, res) => {
  let id = req.params.id;
  //seleccionamos los campos deseados
  let body = _.pick(req.body, ["nombre", "email", "img", "role", "estado"]);

  Usuario.findByIdAndUpdate(
    id,
    body,
    { new: true, runValidators: true, context: "query" },
    (err, usuarioDB) => {
      //validamos...
      if (err) return res.status(500).json({ ok: false, err });
      if (!usuarioDB)
        return res
          .status(400)
          .json({ ok: false, err: { message: "No hay usuarios con ese id" } });

      res.json({ ok: true, usuario: usuarioDB });
    }
  );
});

//desactivar usuario
app.delete("/usuario/:id", [verificarToken, verificarRole], (req, res) => {
  let id = req.params.id;
  let cambiarEstado = { estado: false };

  Usuario.findByIdAndUpdate(
    id,
    cambiarEstado,
    { new: true },
    (err, usuarioEliminadoDB) => {
      //validamos...
      if (err) return res.status(500).json({ ok: false, err });
      //validamos el usuario
      else if (
        !usuarioEliminadoDB ||
        typeof usuarioEliminadoDB === "undefined"
      ) {
        return res
          .status(400)
          .json({ ok: true, err: { mensaje: "Usuario no encontrado" } });
      }

      res.json({ ok: true, usuario: usuarioEliminadoDB });
    }
  );
});
//=============================================== ABM USUARIOS ===============================================

module.exports = app;
