const jwt = require('jsonwebtoken');

//Verificar token
const verificarToken = (req, res, next) => {
    let token = req.get('token');

    jwt.verify(token, process.env.SEED, (err, decoded) =>{
        if(err) return res.status(401).json({ ok: false, err: { message: "Token inválido"} });

        req.usuario = decoded.usuario;
        next();
    });
}

//Verificar role
const verificarRole = (req, res, next) => {
    let usuario = req.usuario;

    if(usuario.role == 'ADMIN_ROLE')  next();
    else {
      return res.status(401).json({ ok: false, err: { message: "Accion válida solamente para administradores"} });
    }
}

//Verificar token via URI
const verificarTokenURI = (req, res, next) =>{
    let token = req.query.token;

    jwt.verify(token, process.env.SEED, (err, decoded) =>{
        if(err) return res.status(401).json({ ok: false, err: { message: "Token inválido"} });

        req.usuario = decoded.usuario;
        next();
    });
}

module.exports = {
    verificarToken,
    verificarRole,
    verificarTokenURI
}